FROM ubuntu:16.04

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 && \
  echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list && \
  apt-get update && \
  apt-get install -y mongodb-org && \
  mkdir -p /data/db && \
  chmod 777 /data/db

COPY entrypoint.sh /

RUN chmod a+x entrypoint.sh

COPY mongod.conf /

EXPOSE 27017

USER 1001

ENTRYPOINT ["/entrypoint.sh"]